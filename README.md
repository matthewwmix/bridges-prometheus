# Description
prometheus a metrics collection tool that has alot of extensibility and flexibility. See [their](https://prometheus.io/) website for more details.

# Building
```bash
./build.sh
```

# Usage
```bash
# The following script will start prometheus and the prometheus pushgateway service on ports 9090 and 9091 respectively.
# Once running you can view some basic status and perform some basic metrics queries by navigating to http://54.167.213.129:9090/graph
./start.sh
```
