FROM centos:7

RUN yum install wget -y
COPY files/prometheus-2.17.2.linux-amd64.tar.gz /
RUN mkdir /prometheus && \
    wget https://github.com/prometheus/prometheus/releases/download/v2.17.2/prometheus-2.17.2.linux-amd64.tar.gz -O /prometheus-2.17.2.linux-amd64.tar.gz && \
    tar -xzvf prometheus-2.17.2.linux-amd64.tar.gz && \
    mv /prometheus-2.17.2.linux-amd64/prometheus /prometheus/prometheus && \
    rm -f prometheus-2.17.2.linux-amd64.tar.gz && \
    rm -rf /prometheus-2.17.2.linux-amd64 

COPY files/startup.sh /

# Expose prometheus
EXPOSE 9090

CMD ["/startup.sh"]
